package test_Package;

import org.testng.annotations.*;

public class TestNG_Class {

	@Test(priority =1, description="This will perform login functionality test")
	public void login_Function() {
		System.out.println("HEllo world");
	}

	@Test(priority = 3, description="This will perform checkout test")
	public void checkout() {
		System.out.println("Checkout");
	}
	
	@Test(priority = 2, description="This will perform add item functionality test")
	public void item_Added() {
		System.out.println("Can");
	}
	@BeforeTest
	public void apple() {
		System.out.println("Before Test");
	}
	@BeforeTest
	public void apple1() {
		System.out.println("Before Test1");
	}
}
